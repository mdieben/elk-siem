# ELK SIEM on Ubuntu 18.04
Author: Michel Dieben

Prerequisites: Ubtuntu 18.04

Follow the steps in the install script (don't run it, there are manual tweaks to be done to config files)

Based on [this guide by Michael Hamblin](https://groups.io/g/nerdhobbychat/topic/elk_and_siem_for_fun_and/32266760?p=,,,20,0,0,0::recentpostdate%2Fsticky,,,20,2,0,32266760)