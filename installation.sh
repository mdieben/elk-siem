# the usual stuff
sudo apt update && sudo apt upgrade -y

################
# Install Java #
################
# Although ELK ís shipped with it's own Java, install this to prevent errors when installing Logstash.

sudo apt install -y openjdk-8-jdk 

# Import the Elasticsearch PGP Key
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -

#########################
# Install Elasticsearch #
#########################

# Installing from the APT repository
sudo apt install apt-transport-https
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
sudo apt update && sudo apt install -y elasticsearch

# Modify the configuration YAML
sudo nano /etc/elasticsearch/elasticsearch.yml

# modify the line:
network.host #example: 10.0.0.1
network.port #example: 9200
# add the line to make sure Elasticsearch doesn't error out when starting the service
cluster.initial_master_nodes: node-1

# Enable auto-startup and then start Elasticsearch
sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable elasticsearch.service
sudo systemctl start elasticsearch.service

# To check the status of the service
sudo systemctl status elasticsearch
curl -X GET "10.0.0.1:9200"

##################
# Install Kibana #
##################
sudo apt install -y kibana

# Modify the configuration YAML
sudo nano /etc/kibana/kibana.yml
# uncomment server.host and change IP address
# uncomment server.name and change name
# edit elasticsearch.host to the elasticsearch settings set before

# Enable auto-startup and then start Kibana
sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable kibana.service
sudo systemctl start kibana.service

####################
# Install Logstash #
####################
sudo apt install -y logstash


#######################
!!!! Client Install !!!
#######################

######################
# Install Metricbeat #
######################

# If on a new system, install the respository first:
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list

# Install Metricbeat
sudo apt install -y metricbeat

# Enable Elasticsearch X-Pack
metricbeat modules enable elasticsearch-xpack

# Modify the yml configuration
sudo nano /etc/metricbeat/metricbeat.yml
# At minimum, specify the output location of Elasticsearch under: output.elasticsearch

# Enable auto-startup and then start Metricbeat
sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable metricbeat.service
sudo systemctl start metricbeat.service


######################
# Install Audit #
######################

# If on a new system, install the respository first:
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list

# Install Auditbeat
sudo apt install -y auditbeat

# Modify the YAML
sudo nano /etc/auditbeat/auditbeat.yml
# Specify the output location of Elasticsearch under: output.elasticsearch and Kibana under setup.kibana

# Setup
sudo auditbeat setup
# All should be well. If there are errors, fix the errors.

# Enable auto-startup and then start Auditbeat
sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable auditbeat.service
sudo systemctl start auditbeat.service

# Enable security on SIEM
sudo nano /etc/elasticsearch/elasticsearch.yml
# add
xpack.security.enabled: true
xpack.security.transport.ssl.enabled: true

# Generate passwords for default users
cd /usr/share/elasticsearch
sudo bin/elasticsearch-setup-passwords auto

# Enter credentials to Kibana.yml
sudo nano /etc/kibana/kibana.yml
elasticsearch.username: "kibana"
elasticsearch.password: "PASSWORD-THAT-WAS-JUST-GENERATED"
xpack.encryptedSavedObjects.encryptionKey: 'ENTER-ANY-ALPHANUMERIC-VALUE-OF-AT-ELAST-32-CHARACTERS'

# Login to the webpage of Kibana with the credentials for user "elastic"