# 1. Setup Suricata in pfSense

Enable EVE from Service – Suricata – Edit interface mapping EVE Output Settings
```
EVE JSON Log [x]
EVE Output Type: File
```

# 2. Install Filebeat FreeBSD package
https://freebsd.pkgs.org/11/freebsd-amd64/beats7-7.7.1.txz.html
Find beats-x.x.x.txz

```
curl -o beats7-7.7.1.txz https://pkg.freebsd.org/FreeBSD:11:amd64/quarterly/All/beats7-7.7.1.txz
pkg add beats7-7.7.1.txz
```

# 3. Download modules
https://www.elastic.co/downloads/past-releases
Download same version Filebeat LINUX 64-BIT

```
curl -o filebeat-7.7.1-linux-x86_64.tar.gz https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.7.1-linux-x86_64.tar.gz
tar zxvf filebeat-7.7.1-linux-x86_64.tar.gz
mv filebeat-7.7.1-linux-x86_64/modules.d /usr/local/etc/beats
mv filebeat-7.7.1-linux-x86_64/module /usr/local/share/beats/filebeat/
mv filebeat-7.7.1-linux-x86_64/kibana /usr/local/sbin/
```

# 4. Configure Filebeat
`nano /usr/local/etc/beats/filebeat.yml`

``` 
#============================= Filebeat modules ===============================
filebeat.modules:
  - module: suricata
    eve:
      enabled: true
      var.paths: ["/var/log/suricata/*/eve.json*"]

logging.to_syslog: false
logging.to_files: true
logging.files:
  path: /var/log/filebeat
  name: filebeat.log
  keepfiles: 7
```
This is the basic configuration. Add your connection details to the Elasticsearch and Kibana server accordingly.

## Create the log directory 
`mkdir /var/log/filebeat`

# 5. Setup Filebeat
## 5.1 Test the configuration
`/usr/local/sbin/filebeat -c /usr/local/etc/beats/filebeat.yml test config`

## 5.2 Setup Filebeat
`/usr/local/sbin/filebeat -c /usr/local/etc/beats/filebeat.yml setup -e`

If dashboards aren't loaded in Kibana run this to see the error message:
`/usr/local/sbin/filebeat -c /usr/local/etc/beats/filebeat.yml setup --dashboards`

## 5.3 Enable Filebeat to run on boot
```
ln -s /usr/local/etc/rc.d/filebeat /usr/local/etc/rc.d/filebeat.sh
echo "filebeat_enable=yes" >> /etc/rc.conf.local
echo "filebeat_conf=/usr/local/etc/beats/filebeat.yml" >> /etc/rc.conf.local
```

# 6. Finish setup in Kibana
The log stream should start apearing now in Kibana. Check out the 2 demo dashboards: Events Overview and Alert Overview.